<?php
namespace Comsa\SuluPageExport\Admin;

use Sulu\Bundle\ActivityBundle\Infrastructure\Sulu\Admin\View\ActivityViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\View\ViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\View\ViewCollection;
use Sulu\Bundle\PageBundle\Admin\PageAdmin as SuluPageAdmin;
use Sulu\Bundle\PageBundle\Teaser\Provider\TeaserProviderPoolInterface;
use Sulu\Component\PHPCR\SessionManager\SessionManagerInterface;
use Sulu\Component\Security\Authorization\SecurityCheckerInterface;
use Sulu\Component\Webspace\Manager\WebspaceManagerInterface;

class PageAdmin extends SuluPageAdmin
{
    const PAGES_VIEW_WITH_EXPORT = 'sulu_page.page_list_with_export';

    public function __construct(ViewBuilderFactoryInterface $viewBuilderFactory, WebspaceManagerInterface $webspaceManager, SecurityCheckerInterface $securityChecker, SessionManagerInterface $sessionManager, TeaserProviderPoolInterface $teaserProviderPool, bool $versioningEnabled, ActivityViewBuilderFactoryInterface $activityViewBuilderFactory)
    {
        parent::__construct($viewBuilderFactory, $webspaceManager, $securityChecker, $sessionManager, $teaserProviderPool, $versioningEnabled, $activityViewBuilderFactory);
    }

    public function configureViews(ViewCollection $viewCollection): void
    {
        parent::configureViews($viewCollection);
        $viewCollection->get(static::PAGES_VIEW)->setType(static::PAGES_VIEW_WITH_EXPORT);
    }
}
