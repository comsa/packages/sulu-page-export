<?php


namespace Comsa\SuluPageExport\Handler;


use Sulu\Bundle\PageBundle\Document\BasePageDocument;

class ExtensionHandler extends BaseHandler
{
    public function handleData(BasePageDocument $document): array
    {
        $extensionData = $document->getExtensionsData()->toArray();

        //-- Handle SEO
        $seoData = $this->handleSEO($extensionData['seo'], $document->getLocale());

        //-- Handle excerpt
        $excerptData = $this->handleExcerpt($extensionData['excerpt'], $document->getLocale());

        //-- Bind data
        $data = array_merge($seoData, $excerptData);

        //-- Cleanup
        foreach ($data as $k => $value) {
            if (!empty($value)) {
                continue;
            }
            unset($data[$k]);
        }
        return $data;
    }

    private function handleSEO(array $seo)
    {
        $data = [];
        $data['seo_title'] = $seo['title'];
        $data['seo_description'] = $seo['description'];
        $data['seo_keywords'] = $seo['keywords'];
        $data['seo_noIndex'] = (bool) $seo['noIndex'];
        $data['seo_noFollow'] = (bool) $seo['noFollow'];
        $data['seo_hideInSitemap'] = (bool) $seo['hideInSitemap'];
        return $data;
    }

    private function handleExcerpt(array $excerpt, string $locale)
    {
        $data = [];
        $data['excerpt_title'] = $excerpt['title'];
        $data['excerpt_more'] = $excerpt['more'];
        $data['excerpt_description'] = $excerpt['description'];
        $data['excerpt_icon'] = $this->handleMedia($excerpt['icon']['ids'], $locale);
        $data['excerpt_images'] = $this->handleMedia($excerpt['images']['ids'], $locale);
        return $data;
    }
}
