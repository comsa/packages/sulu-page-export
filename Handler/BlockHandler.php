<?php
namespace Comsa\SuluPageExport\Handler;

use Sulu\Bundle\MediaBundle\Media\Manager\MediaManagerInterface;
use Sulu\Bundle\PageBundle\Document\BasePageDocument;
use Sulu\Bundle\PageBundle\Document\PageDocument;

class BlockHandler extends BaseHandler
{
    const FIELD_NAMES = ['blocks'];

    protected $fieldNames;

    public function __construct(MediaManagerInterface $mediaManager, array $fieldNames = [])
    {
        $this->fieldNames = $fieldNames;
        parent::__construct($mediaManager);
    }

    public function handleData(BasePageDocument $document): array
    {
        $structure = $document->getStructure()->toArray();

        $data = [];

        $fieldNames = count($this->fieldNames) ? $this->fieldNames : self::FIELD_NAMES;
        foreach ($fieldNames as $fieldName) {
            if (!isset($structure[$fieldName])) {
                continue;
            }
            $data = array_merge($data, $this->handleBlocks($structure[$fieldName], $fieldName, $document->getLocale()));
        }

        return $data;
    }

    private function handleBlocks(array $blocks, string $prefix, string $locale)
    {
        if (empty($blocks)) {
            return [];
        }

        $data = [];

        foreach ($blocks as $k => $block) {
            switch ($block['type']) {
                case 'text':
                    $data[$prefix . '_' . $k] = $block['text'];
                    break;
                case 'images':
                    $data[$prefix . '_' . $k] = $this->handleMedia($block['images']['ids'], $locale);
                    break;
            }
        }
        return $data;
    }
}
