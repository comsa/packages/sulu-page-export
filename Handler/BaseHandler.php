<?php


namespace Comsa\SuluPageExport\Handler;


use Sulu\Bundle\MediaBundle\Media\Manager\MediaManagerInterface;
use Sulu\Bundle\PageBundle\Document\BasePageDocument;

class BaseHandler implements HandlerInterface
{
    private $mediaManager;

    public function __construct(MediaManagerInterface $mediaManager)
    {
        $this->mediaManager = $mediaManager;
    }

    public function handleData(BasePageDocument $document): array {
        $structure = $document->getStructure()->toArray();
        $return = [];
        foreach ($structure as $key => $value) {
            if (is_string($value)) {
                $return[$key] = $value;
            }
        }
        return $return;
    }

    protected function handleMedia(array $ids, string $locale)
    {
        $urls = [];
        foreach ($ids as $id) {
            $media = $this->mediaManager->getById($id, $locale);
            $urls[] = $media->getUrl();
        }
        return implode(',', $urls);
    }
}
