<?php
namespace Comsa\SuluPageExport\Handler;

use Sulu\Bundle\PageBundle\Document\BasePageDocument;

interface HandlerInterface
{
    public function handleData(BasePageDocument $document): array;
}
