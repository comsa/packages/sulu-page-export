import PageList from 'sulu-page-bundle/views/PageList/PageList';
import React, {Fragment} from 'react';
import pageListStyles from 'sulu-page-bundle/views/PageList/pageList.scss';
import {List, ListStore, withToolbar} from 'sulu-admin-bundle/containers';

class PageListWithExport extends PageList {
  // render() {
  //   return (
  //     <div className={pageListStyles.pageList}>
  //       <List
  //         adapterOptions={{
  //           column_list: {
  //             display_root_level_toolbar: false,
  //           },
  //         }}
  //         adapters={['column_list_with_export', 'tree_table']}
  //         onItemAdd={this.handleItemAdd}
  //         onItemClick={this.handleEditClick}
  //         searchable={false}
  //         selectable={false}
  //         store={this.listStore}
  //         toolbarClassName={pageListStyles.listToolbar}
  //       />
  //       {this.cacheClearToolbarAction.getNode()}
  //     </div>
  //   );
  // }
}

export default PageListWithExport;
