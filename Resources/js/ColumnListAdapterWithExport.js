import React from 'react';
import ColumnListAdapter from 'sulu-admin-bundle/containers/List/adapters/ColumnListAdapter';
import columnListAdapterStyles from 'sulu-admin-bundle/containers/List/adapters/columnListAdapter.scss';
import ColumnList from 'sulu-admin-bundle/components/ColumnList';
import {translate} from "sulu-admin-bundle/utils/Translator";
import {action, observable} from 'mobx';
import symfonyRouting from 'fos-jsrouting/router';

export default class ColumnListAdapterWithExport extends ColumnListAdapter {
  getToolbarItems = (index: number) => {
    const {
      activeItems,
      adapterOptions: {
        display_root_level_toolbar: displayRootLevelToolbar = true,
      } = {},
      data,
      onItemAdd,
      onRequestItemCopy,
      onRequestItemDelete,
      onRequestItemMove,
      onRequestItemOrder,
    } = this.props;

    if (!activeItems) {
      throw new Error(
        'The ColumnListAdapter does not work without activeItems. '
        + 'This error should not happen and is likely a bug.'
      );
    }

    if (!displayRootLevelToolbar && !activeItems[index]) {
      return [];
    }

    if (this.orderColumn === index) {
      return [
        {
          icon: 'su-times',
          type: 'button',
          onClick: action(() => {
            this.orderColumn = undefined;
          }),
        },
      ];
    }

    const toolbarItems = [];
    const parentColumn = data[index - 1];
    const parentItem = parentColumn ? parentColumn.find((item) => item.id === activeItems[index]) : undefined;
    const {
      _permissions: {
        add: parentAddPermission = true,
        edit: parentEditPermission = true,
      } = {},
    } = parentItem || {};

    if (onItemAdd && parentAddPermission) {
      toolbarItems.push({
        icon: 'su-plus-circle',
        type: 'button',
        onClick: () => {
          onItemAdd(activeItems[index]);
        },
      });
    }

    const hasActiveItem = activeItems[index + 1] !== undefined;
    const column = data[index];
    const item = column ? column.find((item) => item.id === activeItems[index + 1]) : undefined;
    const {
      _permissions: {
        delete: deletePermission = true,
        edit: editPermission = true,
      } = {},
    } = item || {};

    const settingOptions = [];
    if (onRequestItemDelete) {
      settingOptions.push({
        disabled: !hasActiveItem || !deletePermission,
        label: translate('sulu_admin.delete'),
        onClick: () => {
          const itemId = activeItems[index + 1];
          if (!itemId) {
            throw new Error(
              'An undefined itemId cannot be deleted! This should not happen and is likely a bug.'
            );
          }

          onRequestItemDelete(itemId);
        },
      });
    }

    if (onRequestItemMove) {
      settingOptions.push({
        disabled: !hasActiveItem || !editPermission,
        label: translate('sulu_admin.move'),
        onClick: () => {
          const itemId = activeItems[index + 1];
          if (!itemId) {
            throw new Error(
              'An undefined itemId cannot be deleted! This should not happen and is likely a bug.'
            );
          }

          onRequestItemMove(itemId);
        },
      });
    }

    if (onRequestItemCopy) {
      settingOptions.push({
        disabled: !hasActiveItem || !editPermission,
        label: translate('sulu_admin.copy'),
        onClick: () => {
          const itemId = activeItems[index + 1];
          if (!itemId) {
            throw new Error(
              'An undefined itemId cannot be deleted! This should not happen and is likely a bug.'
            );
          }

          onRequestItemCopy(itemId);
        },
      });
    }

    if (onRequestItemOrder) {
      settingOptions.push({
        disabled: !parentEditPermission,
        label: translate('sulu_admin.order'),
        onClick: action(() => {
          this.orderColumn = index;
        }),
      });
    }

    settingOptions.push({
      label: 'Export',
      onClick: () => {
        fetch(symfonyRouting.generate('comsa_sulu_page_export.get_page_export', { id: activeItems[index + 1], _format: 'csv' }))
          .then(res => res.blob())
          .then(blob => {
            const href = window.URL.createObjectURL(blob);
            const a = document.createElement('a');
            a.download = 'Export van pagina\'s';
            a.href = href;
            a.click();
            a.href = '';
          });
      }
    })

    if (settingOptions.length > 0) {
      toolbarItems.push({
        icon: 'su-cog',
        type: 'dropdown',
        options: settingOptions,
      });
    }

    return toolbarItems.length > 0 ? toolbarItems : undefined;
  }

  render() {
    const {
      activeItems,
      disabledIds,
      loading,
      selections,
    } = this.props;

    return (
      <div className={columnListAdapterStyles.columnListAdapter}>
        <ColumnList
          onItemClick={this.handleItemClick}
          onItemDoubleClick={this.handleItemDoubleClick}
          toolbarItemsProvider={this.getToolbarItems}
        >
          {this.props.data.map((items, index) => (
            <ColumnList.Column
              key={index}
              loading={index >= this.props.data.length - 1 && loading}
            >
              {items.map((item: Object, itemIndex: number) => (
                // TODO: Don't access hasChildren, published, publishedState, title or type directly
                <ColumnList.Item
                  active={activeItems ? activeItems.includes(item.id) : undefined}
                  buttons={this.getButtons(item)}
                  disabled={disabledIds.includes(item.id)}
                  hasChildren={item.hasChildren}
                  id={item.id}
                  indicators={this.getIndicators(item)}
                  key={item.id}
                  onOrderChange={this.handleOrderChange}
                  order={itemIndex + 1}
                  selected={selections.includes(item.id)}
                  showOrderField={this.orderColumn === index}
                >
                  {item.title || item.name}
                </ColumnList.Item>
              ))}
            </ColumnList.Column>
          ))}
        </ColumnList>
      </div>
    );
  }
}
