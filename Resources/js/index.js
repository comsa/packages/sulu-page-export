import {viewRegistry} from 'sulu-admin-bundle/containers';
import PageListWithExport from './PageListWithExport';
import {listAdapterRegistry} from "sulu-admin-bundle/containers/List";
import ColumnListAdapterWithExport from './ColumnListAdapterWithExport';

listAdapterRegistry.add('column_list_with_export', ColumnListAdapterWithExport);
viewRegistry.add('sulu_page.page_list_with_export', PageListWithExport);
