Installation
============

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer require comsa/sulu-page-export dev-master
```

### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    Comsa\SuluPageExport\SuluPageExportBundle::class => ['all' => true],
];
```

### Step 3: Active routes for bundle
```yaml
#config/routes_admin.yaml
comsa_sulu_page_export:
    type: rest
    prefix: /admin/api
    resource: '@SuluPageExportBundle/Resources/config/routes.yaml'
```

### Step 4: Add JS to webpack build process

In `assets/admin/package.json` add the page export dependencies like so:

```json
{
  // ...
    "dependencies": {
        // ...
        "sulu-page-export-bundle": "file:../../vendor/comsa/sulu-page-export/Resources/js"
    }
}
```
Install these assets using `npm install`

### Step 5: Add Page export bundle package to admin panel
In `assets/admin/index.js` add the following above `startAdmin()`:

```js
// ...
// Implement custom extensions here
import 'sulu-page-export-bundle';
// ...
```
