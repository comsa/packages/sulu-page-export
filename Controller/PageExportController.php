<?php
namespace Comsa\SuluPageExport\Controller;

use Comsa\SuluPageExport\Handler\HandlerInterface;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Sulu\Bundle\MediaBundle\Api\Media;
use Sulu\Bundle\MediaBundle\Media\Manager\MediaManagerInterface;
use Sulu\Bundle\PageBundle\Document\BasePageDocument;
use Sulu\Bundle\PageBundle\Document\HomeDocument;
use Sulu\Bundle\PageBundle\Document\PageDocument;
use Sulu\Component\DocumentManager\DocumentManagerInterface;
use Sulu\Component\Rest\AbstractRestController;
use Sulu\Component\Rest\Csv\CsvHandler;
use Sulu\Component\Rest\ListBuilder\CollectionRepresentation;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PageExportController extends AbstractRestController
{
    private $documentManager;
    private $entityManager;
    private $mediaManager;
    private $handlers;

    public function __construct(
        ViewHandlerInterface $viewHandler,
        DocumentManagerInterface $documentManager,
        EntityManagerInterface $entityManager,
        MediaManagerInterface $mediaManager,
        iterable $handlers
    )
    {
        parent::__construct($viewHandler);
        $this->documentManager = $documentManager;
        $this->entityManager = $entityManager;
        $this->mediaManager = $mediaManager;
        $this->handlers = $handlers;
    }

    public function getPageExportAction($id)
    {
        /** @var PageDocument $pageDocument */
        $pageDocument = $this->documentManager->find($id);
        $children = $this->getDeepestChildren($pageDocument);

        //-- Make sure all headers are available
        $possibleHeaders = [];
        foreach ($children as $child) {
            $headers = array_keys($child);
            foreach ($headers as $header) {
                if (in_array($header, $possibleHeaders)) {
                    continue;
                }
                $possibleHeaders[] = $header;
            }
        }

        foreach ($children as &$child) {
            foreach ($possibleHeaders as $possibleHeader) {
                if (!isset($child[$possibleHeader])) {
                    $child[$possibleHeader] = '';
                }
            }

            uksort($child, function($a, $b) {
                if ($a === 'title') {
                    return false;
                }

                if ($a === 'url' && $b !== 'title') {
                    return false;
                }

                if ($b === 'url' && $a !== 'title') {
                    return true;
                }

                return strnatcasecmp($a, $b);
            });
        }

        $childrenCollection = new CollectionRepresentation($children, 'export-' . $id);

        $view = $this->view($childrenCollection);
        return $this->handleView($view);
    }

    private function getDeepestChildren(BasePageDocument $pageDocument)
    {
        $children = $pageDocument->getChildren()->toArray();
        $topChildren = [];
        /** @var PageDocument $child */
        foreach ($children as $child) {
            if ($child->getChildren()->count() > 0) {
                $topChildren = array_merge($topChildren, $this->getDeepestChildren($child));
            } else {
                $return = [];
                /** @var HandlerInterface $handler */
                foreach ($this->handlers as $handler) {
                    $data = $handler->handleData($child);
                    if (!empty($data)) {
                        $return = array_merge($return, $data);
                    }
                }
                if (empty($return)) {
                    continue;
                }
                $topChildren[] = $return;
                continue;

                $data = $child->getStructure()->toArray();
                $return = [];

                //-- Extract features
                if (isset($data['features'])) {
                    $featureRepository = $this->entityManager->getRepository(Feature::class);
                    foreach ($data['features'] as $featureCouple) {
                        $feature = $featureRepository->find($featureCouple['linkedFeature']);
                        $return[$feature->getTranslation($child->getLocale())->getTitle()] = $featureCouple['value'];
                    }

                    unset($data['features']);
                }


                if (isset($data['blocks'])) {
                    foreach ($data['blocks'] as $k => $block) {
                        if ($block['type'] === 'media') {
                            $return['blocks-' . $k] = 'Media #' . $block['link']['id'];
                        }

                        if (in_array($block['type'], ['text'])) {
                            $return['blocks-' . $k] = $block['text'];
                        }
                    }
                    unset($data['blocks']);
                }

                if (isset($data['blocks_info'])) {
                    foreach ($data['blocks_info'] as $k => $block) {
                        if ($block['type'] === 'media') {
                            $return['blocks-info-' . $k] = $this->getMediaLinkById($block['link']['id']);
                        }

                        if (in_array($block['type'], ['text'])) {
                            $return['blocks-info-' . $k] = $block['text'];
                        }
                    }
                    unset($data['blocks_info']);
                }

                $extensionsData = $child->getExtensionsData()->toArray();
                //-- Add SEO data
                foreach ($extensionsData['seo'] as $title => $value) {
                    if (strlen($value)) {
                        $return['seo_' . $title] = $value;
                    }
                }
                //-- Excerpt data
                foreach ($extensionsData['excerpt'] as $title => $value) {
                    if (empty($value)) {
                        continue;
                    }
                    if (in_array($title, ['icon', 'images'])) {
                        if (empty($value['ids'])) {
                            continue;
                        }
                        foreach ($value['ids'] as $k => $id) {
                            $return['excerpt_' . $title . '_' . $k] = $this->getMediaLinkById($id, $child->getLocale());
                        }
                    } else {
                        $return['excerpt_' . $title] = $value;
                    }
                }

                $return = array_merge($data, $return);
                $topChildren[] = $return;
            }
        }
        return $topChildren;
    }

    private function getMediaLinkById(int $id, string $locale): string
    {
        $media = $this->mediaManager->getById($id, $locale);
        if (!$media instanceof Media) {
            return '';
        }
        return $media->getFormats()['large'];
    }
}
